import { CityWeather } from 'src/app/models/city-weather';
import { createReducer, on } from '@ngrx/store';
import {
  removeCityWeather,
  updateCityWeather,
} from 'src/app/models/weather.actions';
export const inialState: CityWeather[] = [];

const _weatherReducer = createReducer(
  inialState,
  on(updateCityWeather, (state: any[], { cityWeather }) => {
    const index = state.findIndex((c) => c.City == cityWeather.City);
    if (index == -1) {
      return [...state, cityWeather];
    } else {
      let temp: any = [];
      if (index > 0) {
        let temp1 = state.slice(0, index);
        let temp2 = state.slice(index + 1, state.length);
        temp = temp1.concat(temp2);
      } else {
        temp = state.slice(1, state.length);
      }
      return [...temp, cityWeather];
    }
  }),
  on(removeCityWeather, (state: any[], { cityName }) => {
    const index = state.findIndex((c) => c.City.Name == cityName);
    if (index == -1) {
      return [...state];
    } else {
      let temp1 = state.slice(0, index);
      let temp2 = state.slice(index + 1, state.length);
      return temp1.concat(temp2);
    }
  })
);

export function weatherReducer(state: CityWeather[] | undefined, action: any) {
  return _weatherReducer(state, action);
}
