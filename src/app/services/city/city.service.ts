import { AppState } from './../../models/app-state';
import { CityWeather } from './../../models/city-weather';
import { WeatherService } from './../weather/weather.service';
import cities from 'src/assets/city-list.json';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  removeCityWeather,
  updateCityWeather,
} from 'src/app/models/weather.actions';
import { selectedCities } from 'src/app/models/weather.selectors';

@Injectable({
  providedIn: 'root',
})
export class CityService {
  constructor(private store: Store, private weatherService: WeatherService) {}

  public getAddCity(cityName: string): CityWeather {
    let initCity: CityWeather = {
      City: {},
      Weather: {},
    };
    let newCity = cities.filter((value: any) => {
      return value.Name == cityName;
    });
    if (newCity[0]?.Name != null) {
      initCity.City = newCity[0];
      this.weatherService.getCityWeather(cityName).subscribe((o: any) => {
        Object.assign(initCity.Weather, o.main);
        Object.assign(
          (initCity.Weather.description = o.weather[0].description)
        );
        Object.assign((initCity.Weather.icon = o.weather[0].icon));
        this.store.dispatch(updateCityWeather({ cityWeather: initCity }));
      });
    }
    return initCity;
  }

  public removeCity(cityName: string) {
    this.store.dispatch(removeCityWeather({ cityName }));
  }

  public getCityList(): CityWeather[] {
    let cityList: CityWeather[] = [];
    this.store.select(selectedCities).subscribe((o: any) => {
      cityList = o;
    });
    return cityList;
  }
}
