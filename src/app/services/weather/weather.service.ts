import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class WeatherService {
  constructor(private http: HttpClient) {}

  getCityWeather(cityName: string): any {
    return this.http.get<any>(
      `http://api.openweathermap.org/data/2.5/weather?q=${cityName}&appid=2c989a5ea1c85d27fb513bbda5728ca9`
    );
  }
}
