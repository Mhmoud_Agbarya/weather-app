import { CityWeather } from './../../models/city-weather';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-city-view',
  templateUrl: './city-view.component.html',
  styleUrls: ['./city-view.component.css'],
})
export class CityViewComponent implements OnInit {
  @Input() city: CityWeather = { City: {}, Weather: {} };
  @Output() updateWeather: EventEmitter<string> = new EventEmitter();
  @Output() removeCityWeather: EventEmitter<string> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  onUpdateClick() {
    this.updateWeather.emit(this.city.City.Name);
  }

  onRemoveClick() {
    this.removeCityWeather.emit(this.city.City.Name);
  }
}
