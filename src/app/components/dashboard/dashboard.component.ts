import { CityWeather } from './../../models/city-weather';
import { CityService } from './../../services/city/city.service';
import { WeatherService } from './../../services/weather/weather.service';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectedCities } from 'src/app/models/weather.selectors';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  // selectdCity: Observable<City>;
  public cityList: CityWeather[] = [];

  constructor(
    private store: Store<CityWeather>,
    private weatherService: WeatherService,
    private cityService: CityService
  ) {
    this.store.select(selectedCities).subscribe((o: any) => {
      this.cityList = o;
    });
  }

  ngOnInit(): void {}

  addCity(cityName: string) {
    this.cityService.getAddCity(cityName);
  }

  onRemoveCity(cityName: string) {
    this.cityService.removeCity(cityName);
  }
}
