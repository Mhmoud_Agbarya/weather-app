import { CityWeather } from 'src/app/models/city-weather';
import { createAction, props } from '@ngrx/store';

export const updateCityWeather = createAction(
  '[CityView Component] UpdateCityWeather',
  props<{ cityWeather: CityWeather }>()
);

export const removeCityWeather = createAction(
  '[CityView Component] RemoveCityWeather',
  props<{ cityName: string }>()
);
