import { createSelector } from '@ngrx/store';
import { AppState } from './app-state';

export const selectAllCities = (state: any) => {
  return state;
};

export const selectedCities = createSelector(
  selectAllCities,
  (state: AppState) => {
    return [...state.weatherState];
  }
);
