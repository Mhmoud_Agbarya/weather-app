import { CityWeather } from 'src/app/models/city-weather';

export interface AppState {
  weatherState: CityWeather[];
}
