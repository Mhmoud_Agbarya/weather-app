import { City } from 'src/app/models/city';
import { Weather } from './weather';
export interface CityWeather {
  City: City;
  Weather: Weather;
}
